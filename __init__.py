from trytond.pool import Pool

__all__ = ['register']


def register():
    Pool.register(
        module='account_ad', type_='model')
    Pool.register(
        module='account_ad', type_='wizard')
    Pool.register(
        module='account_ad', type_='report')
