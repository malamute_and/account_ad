===================
Account Ad Scenario
===================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules

Install account_ad::

    >>> config = activate_modules('account_ad')
