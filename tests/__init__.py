try:
    from trytond.modules.account_ad.tests.test_account_ad import suite  # noqa: E501
except ImportError:
    from .test_account_ad import suite

__all__ = ['suite']
